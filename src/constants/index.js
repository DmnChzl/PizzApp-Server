const HELLO_WORLD = 'HelloWorld';

// Colors
const BLUE = '#209CEE';
const GREEN = '#48C774';
const YELLOW = '#FFDD57';
const RED = '#FF3860';

// Errors
const IS_MISSING = 'Is Missing !';
const NOT_AN_ARRAY = 'Not An Array !';
const IS_EMPTY = 'Is Empty !';
const NO_ACCOUNT = 'No Account !';
const NO_PIZZA = 'No Pizza !';
const PSWD_FAILURE = 'PSWD Failure !';

module.exports = {
  HELLO_WORLD,
  BLUE,
  GREEN,
  YELLOW,
  RED,
  IS_MISSING,
  NOT_AN_ARRAY,
  IS_EMPTY,
  NO_ACCOUNT,
  NO_PIZZA,
  PSWD_FAILURE
};
