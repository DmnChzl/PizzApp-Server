const { hashSync } = require('bcryptjs');
const { mockReq, mockRes, connect, clear, disconnect } = require('../setupTests');
const { registerAccount, loginAccount, pswdAccount, getAllAccounts, getAccount, updateAccount, clearAccount } = require('./accountCtrl');
const accountModel = require('../models/accountModel');
const { bsonToJson } = require('../utils');

describe('accountCtrl', () => {
  beforeAll(async () => await connect());

  afterEach(async () => await clear());

  afterAll(async () => await disconnect());

  describe('registerAccount', () => {
    it('Bad Request', async () => {
      const req = mockReq();
      const res = mockRes();

      await registerAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(400);
    });

    it('Created', async () => {
      const req = mockReq(
        {},
        {
          login: 'Test',
          email: 'lorem.ipsum@pm.me',
          password: '123456',
          firstName: 'Lorem',
          lastName: 'Ipsum',
          gender: 'F',
          yearOld: 42
        }
      );

      const res = mockRes();

      await registerAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(201);
    });
  });

  describe('loginAccount', () => {
    it('Bad Request', async () => {
      const req = mockReq();
      const res = mockRes();

      await loginAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(400);
    });

    it('Not Found', async () => {
      const req = mockReq(
        {},
        {
          login: 'Test',
          password: '123456'
        }
      );

      const res = mockRes();

      await loginAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(404);
    });

    it('Unauthorized', async () => {
      await accountModel.create({
        login: 'Test',
        email: 'lorem.ipsum@pm.me',
        password: '123456',
        firstName: 'Lorem',
        lastName: 'Ipsum',
        gender: 'F',
        yearOld: 42
      });

      const req = mockReq(
        {},
        {
          login: 'Test',
          password: '123456'
        }
      );

      const res = mockRes();

      await loginAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(401);
    });

    it('Success', async () => {
      const hashedPswd = hashSync('123456', 10);

      await accountModel.create({
        login: 'Test',
        email: 'lorem.ipsum@pm.me',
        password: hashedPswd,
        firstName: 'Lorem',
        lastName: 'Ipsum',
        gender: 'F',
        yearOld: 42
      });

      const req = mockReq(
        {},
        {
          login: 'Test',
          password: '123456'
        }
      );

      const res = mockRes();

      await loginAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
    });
  });

  describe('pswdAccount', () => {
    it('Bad Request', async () => {
      const req = mockReq();
      const res = mockRes();

      await pswdAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(400);
    });

    it('Not Found', async () => {
      const req = mockReq(
        {},
        {
          oldPswd: '123456',
          newPswd: 'ABCDEF'
        },
        {
          decodedId: 'ABCDEF123456'
        }
      );

      const res = mockRes();

      await pswdAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(404);
    });

    it('Unauthorized', async () => {
      const result = await accountModel.create({
        login: 'Test',
        email: 'lorem.ipsum@pm.me',
        password: '123456',
        firstName: 'Lorem',
        lastName: 'Ipsum',
        gender: 'F',
        yearOld: 42
      });

      const req = mockReq(
        {},
        {
          oldPswd: '123456',
          newPswd: 'ABCDEF'
        },
        {
          decodedId: bsonToJson(result).id
        }
      );

      const res = mockRes();

      await pswdAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(401);
    });

    it('Success', async () => {
      const hashedPswd = hashSync('123456', 10);

      const result = await accountModel.create({
        login: 'Test',
        email: 'lorem.ipsum@pm.me',
        password: hashedPswd,
        firstName: 'Lorem',
        lastName: 'Ipsum',
        gender: 'F',
        yearOld: 42
      });

      const req = mockReq(
        {},
        {
          oldPswd: '123456',
          newPswd: 'ABCDEF'
        },
        {
          decodedId: bsonToJson(result).id
        }
      );

      const res = mockRes();

      await pswdAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
    });
  });

  it('getAllAccounts', async () => {
    const req = mockReq();
    const res = mockRes();

    await getAllAccounts(req, res);

    expect(res.status).toHaveBeenCalledWith(200);
  });

  describe('getAccount', () => {
    it('Not Found', async () => {
      const req = mockReq({}, {}, { decodedId: 'ABCDEF123456' });
      const res = mockRes();

      await getAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(404);
    });

    it('Success', async () => {
      const result = await accountModel.create({
        login: 'Test',
        email: 'lorem.ipsum@pm.me',
        password: '123456',
        firstName: 'Lorem',
        lastName: 'Ipsum',
        gender: 'F',
        yearOld: 42
      });

      const req = mockReq({}, {}, { decodedId: bsonToJson(result).id });
      const res = mockRes();

      await getAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
    });
  });

  describe('updateAccount', () => {
    it('Bad Request', async () => {
      const req = mockReq({}, {}, { decodedId: 'ABCDEF123456' });
      const res = mockRes();

      await updateAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(400);
    });

    it('Not Found', async () => {
      const req = mockReq({}, { yearOld: 24 }, { decodedId: 'ABCDEF123456' });
      const res = mockRes();

      await updateAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(404);
    });

    it('Success', async () => {
      const result = await accountModel.create({
        login: 'Test',
        email: 'lorem.ipsum@pm.me',
        password: '123456',
        firstName: 'Lorem',
        lastName: 'Ipsum',
        gender: 'F',
        yearOld: 42
      });

      const req = mockReq({}, { yearOld: 24 }, { decodedId: bsonToJson(result).id });
      const res = mockRes();

      await updateAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
    });
  });

  describe('clearAccount', () => {
    it('Not Found', async () => {
      const req = mockReq({}, {}, { decodedId: 'ABCDEF123456' });
      const res = mockRes();

      await clearAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(404);
    });

    it('Success', async () => {
      const result = await accountModel.create({
        login: 'Test',
        email: 'lorem.ipsum@pm.me',
        password: '123456',
        firstName: 'Lorem',
        lastName: 'Ipsum',
        gender: 'F',
        yearOld: 42
      });

      const req = mockReq({}, {}, { decodedId: bsonToJson(result).id });
      const res = mockRes();

      await clearAccount(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
    });
  });
});
