const { mockReq, mockRes, connect, clear, disconnect } = require('../setupTests');
const { createManyPizzas, createPizza, getAllPizzas, getPizza, updatePizza, clearAllPizzas, clearPizza } = require('./pizzaCtrl');
const pizzaModel = require('../models/pizzaModel');
const { bsonToJson } = require('../utils');

describe('pizzaCtrl', () => {
  beforeAll(async () => await connect());

  afterEach(async () => await clear());

  afterAll(async () => await disconnect());

  describe('createManyPizzas', () => {
    it('Bad Request', async () => {
      const req = mockReq();
      const res = mockRes();

      await createManyPizzas(req, res);

      expect(res.status).toHaveBeenCalledWith(400);
    });

    it('Created', async () => {
      const req = mockReq({}, [
        {
          label: 'Test I',
          items: ['One'],
          price: 1.0
        },
        {
          label: 'Test II',
          items: ['One', 'Two'],
          price: 2.0
        },
        {
          label: 'Test III',
          items: ['One', 'Two', 'Three'],
          price: 3.0
        }
      ]);

      const res = mockRes();

      await createManyPizzas(req, res);

      expect(res.status).toHaveBeenCalledWith(201);
    });
  });

  describe('createPizza', () => {
    it('Bad Request', async () => {
      const req = mockReq();
      const res = mockRes();

      await createPizza(req, res);

      expect(res.status).toHaveBeenCalledWith(400);
    });

    it('Created', async () => {
      const req = mockReq(
        {},
        {
          label: 'Lorem Ipsum',
          items: ['One', 'Two', 'Three'],
          price: 9.9
        }
      );

      const res = mockRes();

      await createPizza(req, res);

      expect(res.status).toHaveBeenCalledWith(201);
    });
  });

  it('getAllPizzas', async () => {
    const req = mockReq();
    const res = mockRes();

    await getAllPizzas(req, res);

    expect(res.status).toHaveBeenCalledWith(200);
  });

  describe('getPizza', () => {
    it('Bad Request', async () => {
      const req = mockReq();
      const res = mockRes();

      await getPizza(req, res);

      expect(res.status).toHaveBeenCalledWith(400);
    });

    it('Not Found', async () => {
      const req = mockReq({ id: 'ABCDEF123456' });
      const res = mockRes();

      await getPizza(req, res);

      expect(res.status).toHaveBeenCalledWith(404);
    });

    it('Success', async () => {
      const result = await pizzaModel.create({
        label: 'Test',
        items: ['One', 'Two', 'Three'],
        price: 9.99
      });

      const req = mockReq({ id: bsonToJson(result).id });
      const res = mockRes();

      await getPizza(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
    });
  });

  describe('updatePizza', () => {
    describe('Bad Request', () => {
      it("'ID' Is Missing", async () => {
        const req = mockReq();
        const res = mockRes();

        await updatePizza(req, res);

        expect(res.status).toHaveBeenCalledWith(400);
      });

      it('Is Empty', async () => {
        const req = mockReq({ id: 'ABCDEF123456' });
        const res = mockRes();

        await updatePizza(req, res);

        expect(res.status).toHaveBeenCalledWith(400);
      });
    });

    it('Not Found', async () => {
      const req = mockReq({ id: 'ABCDEF123456' }, { label: 'Test' });
      const res = mockRes();

      await updatePizza(req, res);

      expect(res.status).toHaveBeenCalledWith(404);
    });

    it('Success', async () => {
      const result = await pizzaModel.create({
        label: 'Lorem Ipsum',
        items: ['One', 'Two', 'Three'],
        price: 9.99
      });

      const req = mockReq({ id: bsonToJson(result).id }, { label: 'Test' });
      const res = mockRes();

      await updatePizza(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
    });
  });

  it('clearAllPizzas', async () => {
    const req = mockReq();
    const res = mockRes();

    await clearAllPizzas(req, res);

    expect(res.status).toHaveBeenCalledWith(200);
  });

  describe('clearPizza', () => {
    it('Bad Request', async () => {
      const req = mockReq();
      const res = mockRes();

      await clearPizza(req, res);

      expect(res.status).toHaveBeenCalledWith(400);
    });

    it('Not Found', async () => {
      const req = mockReq({ id: 'ABCDEF123456' });
      const res = mockRes();

      await clearPizza(req, res);

      expect(res.status).toHaveBeenCalledWith(404);
    });

    it('Success', async () => {
      const result = await pizzaModel.create({
        label: 'Lorem Ipsum',
        items: ['One', 'Two', 'Three'],
        price: 9.99
      });

      const req = mockReq({ id: bsonToJson(result).id });
      const res = mockRes();

      await clearPizza(req, res);

      expect(res.status).toHaveBeenCalledWith(200);
    });
  });
});
