const { connect, clear, disconnect } = require('../setupTests');
const accountModel = require('./accountModel');

describe('accountModel', () => {
  beforeAll(async () => await connect());

  afterEach(async () => await clear());

  afterAll(async () => await disconnect());

  it('Should Be Created', async () => {
    const result = await accountModel.create({
      login: 'Test',
      email: 'lorem.ipsum@pm.me',
      password: '123456',
      firstName: 'Lorem',
      lastName: 'Ipsum',
      gender: 'F',
      yearOld: 42
    });

    expect(result.login).toEqual('Test');
    expect(result.email).toEqual('lorem.ipsum@pm.me');
    expect(result.password).toEqual('123456');
    expect(result.firstName).toEqual('Lorem');
    expect(result.lastName).toEqual('Ipsum');
    expect(result.gender).toEqual('F');
    expect(result.yearOld).toEqual(42);
  });
});
