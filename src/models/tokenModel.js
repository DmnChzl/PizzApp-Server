const { Schema, model } = require('mongoose');

/**
 * Token Model
 *
 * @param {String} secret Secret
 * @param {Date} expirationDate Expiration Date
 */
const tokenSchema = new Schema(
  {
    secret: {
      type: String,
      unique: true
    },
    expirationDate: Date
  },
  {
    versionKey: false
  }
);

module.exports = model('tokens', tokenSchema);
