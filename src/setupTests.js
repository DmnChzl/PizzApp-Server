const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');

const mongod = new MongoMemoryServer();

/**
 * Mock Request
 *
 * @param {Object} params Params
 * @param {Object} body Body
 * @param {Object} headers Headers
 */
const mockReq = (params = {}, body = {}, headers = {}) => ({
  ...headers,
  params,
  body
});

/**
 * Mock Response
 */
const mockRes = () => {
  const res = {};

  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);

  return res;
};

/**
 * Mock For MongoDB 'connect'
 */
const connect = async () => {
  const uri = await mongod.getConnectionString();

  await mongoose.connect(uri, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
};

const clear = async () => {
  const collections = mongoose.connection.collections;

  for (const key in collections) {
    const collection = collections[key];
    await collection.deleteMany();
  }
};

/**
 * Mock For MongoDB 'disconnect'
 */
const disconnect = async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
  await mongod.stop();
};

module.exports = {
  mockReq,
  mockRes,
  connect,
  clear,
  disconnect
};
