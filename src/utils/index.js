const { accessSync, readFileSync } = require('fs');
const { resolve } = require('path');
const { networkInterfaces } = require('os');
const { IS_MISSING } = require('../constants');

/**
 * Is Defined
 *
 * @param {Any} data Data
 * @returns {Boolean} Result
 */
const isDefined = data => {
  if (data === undefined || data === null) {
    return false;
  }

  return true;
};

/**
 * BSON To JSON
 *
 * @param {Object} document Document
 * @returns {Object} Formatted Document
 */
const bsonToJson = ({ _doc: doc }) => {
  // eslint-disable-next-line
  const { _id: id, _class, ...data } = doc;

  return {
    id,
    ...data
  };
};

/**
 * Is File Exist
 *
 * @param {String} fileName FileName
 * @returns {Boolean} Result
 */
const isFileExist = fileName => {
  try {
    accessSync(resolve(__dirname, '..', fileName));
  } catch (e) {
    // console.log(e);
    return false;
  }

  return true;
};

/**
 * Read JSON File
 *
 * @param {String} fileName FileName
 * @returns {Any} JSON
 */
const readJsonFile = fileName => {
  const content = readFileSync(resolve(__dirname, '..', fileName));

  return JSON.parse(content);
};

/**
 * Get IP Addresses
 *
 * @returns {Array} ID Addresses
 */
const getAddresses = () => {
  const iFaces = networkInterfaces();

  return Object.keys(iFaces).flatMap(ifName => {
    return iFaces[ifName].filter(({ family }) => family === 'IPv4').map(iFace => iFace.address);
  });
};

/**
 * ShortCut: Is Missing
 *
 * @param {String} property Property
 * @returns {String} Text
 */
const isMissing = property => `'${property}' ${IS_MISSING}`;

module.exports = {
  isDefined,
  bsonToJson,
  isFileExist,
  readJsonFile,
  getAddresses,
  isMissing
};
